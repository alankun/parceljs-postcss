/*
commonJS syntax
const jokes = require('./jokes')
*/

// ES6 modules syntax
import {jokes} from './jokes'

jokes.getOne()
	.then(joke => document.getElementById("joke").innerHTML = joke)